/*******Author: Roccon Manuel - manuel.roccon@gmail.com********************/
/************Last modify: 18/10/2017***********/
/************************************************/
//for corrert use of this script must insert this library in arduino ide
#include <Servo.h>
#include <EEPROM.h>
#include <OneWire.h>
#include <LiquidCrystal_I2C.h>

/*
Conector pin detail:
A0: up photo sensor
A1: down photo sensor
A3: right photo sensor
A2: left photo sensor
D9: servo axis X (right left motor)
D10: servo axix Y (up down motor)
SCL: scl display pin (in adruino uno is near reset button)
SDA: sda display pin (in adruino uno is near reset button)
 */
 
LiquidCrystal_I2C lcd(0x20,16,2);  // set the LCD address to 0x20(Cooperate with 3 short circuit caps) for a 16 chars and 2 line display
/************************************************/
Servo servoX;//create servo object to control a servo
Servo servoY;//create servo object to control a servo
Servo myServo;
/************************************************/

//define sensor pin position
const int photoUpPin = A0;  //up analog pin to attach sensor
const int photoBottomPin = A1;   //down analog pin to attach sensor
const int photoRightPin = A3;  //right analog pin to attach sensor
const int photoLeftPin = A2;  //left analog pin to attach sensor


//min and max range of servo
const int xMin = 20; //Max angle more= less left angle
const int xMax = 180;//Max angle less= less right angle
const int yMin = 90; //Max angle more= less top angle
const int yMax = 180; //Max angle less= less bottom angle

//servo speed parameter
const int lowSpeed = 1; //the lowest speed of servo
const int divisor = 100; //this value control the variation of speed if the difference of 2 sensor is low or hight (more = slower)
int tollerance = 10;    //sensibility of sensor - increse if servo continuos correct the angle, decrese if not much sensible of variation
int spd = 0; //no touch
int diff = 0; //no touch (variable used to calcolate diffenrence between raw sensors data)
int AccLevel = 0; //no touch (variable used to calcolate de speed of ajustemnt moevment)

//variable of degree of X Y
int degreeX = 90;
int degreeY = 90;

//value of light up down right left
int lightTop = 0;
int lightRight = 0;
int lightBottom = 0;
int lightLeft = 0;

//minvalue of 4 sensor to activate the system
int lightAvMin = 0;

void setup() {
   lcd.init();                      // initialize the lcd 
  lcd.backlight();
  
  lcd.home();
  
  // put your setup code here, to run once:
  servoX.attach(9);//attachs the servo on pin 9 to servo object
  servoY.attach(10);//attachs the servo on pin 9 to servo object

  //this avoid servo default angle at the start
  degreeY = EEPROM.read(1);
  servoY.write(degreeY);
  degreeX =  EEPROM.read(10);
  servoX.write(degreeX);

  delay(1000);//wait for a second
  Serial.begin(19200);
  degreeY = servoY.read();
  degreeX = servoX.read();
}

void loop() {
  // put your main code here, to run repeatedly:
  lightTop = analogRead(photoUpPin);
  lightBottom = analogRead(photoBottomPin)* 1.15; //this to ajust tollerance between sensor (each sensor have oun tollerance)

  Serial.print("\nUpSensor: ");
  Serial.print(lightTop);
  Serial.print("\tDownSensor: ");
  Serial.print(lightBottom);


  Serial.flush();
  if (lightTop < lightAvMin) {
    //standbye
    Serial.print("\tSTANDBYE LUX<");  Serial.print(lightAvMin);
    //servoY.write(80);
  } else {
    //activate
    diff = abs(lightTop - lightBottom); // Difference between 2 sensors
    spd = diff / divisor; // speed for moviment (more difference + speed, less differnece more acuracy)
    AccLevel = lowSpeed + spd;
   lcd.setCursor(0, 0);
    if ((lightTop < (lightBottom + tollerance) and lightTop > (lightBottom - tollerance))) {
      AccLevel = 0;
      Serial.print("\tSTOP:Y-PAUSE ");
      
      lcd.print("STOP:Y-PAUSE  ");
    } else if (lightTop > lightBottom and degreeY > yMin) {
      degreeY = degreeY - AccLevel;
      Serial.print("\tMOVING:Y-UP ");
      lcd.print("MOVING:UP   ");
    } else if (lightTop < lightBottom and degreeY < yMax) {
      degreeY = degreeY + AccLevel;
      Serial.print("\tMOVING:Y-DOWN ");
      lcd.print("MOVING:DOWN   ");
    } else {
      Serial.print("\tSTOP:Y-OFFSET");
      lcd.print("STOP:Y-OFFSET   ");
    }
    Serial.print("\tY-Speed: "); Serial.print(AccLevel);
    Serial.print("\tY-Degree: "); Serial.print(degreeY - 80);
    servoY.write(degreeY);
    EEPROM.write( 1, degreeY);
    delay(0);

    //activate
    lcd.setCursor(0, 1);
    lightRight = analogRead(photoRightPin)*1.06;
    lightLeft = analogRead(photoLeftPin);
    Serial.print("\tRightSensor: ");
    Serial.print(lightRight);
    Serial.print("\tLeftSensor: ");
    Serial.print(lightLeft);
    diff = abs(lightRight - lightLeft); 
    spd = diff / divisor; 
    AccLevel = lowSpeed + spd;
    
    if ((lightRight < (lightLeft + tollerance) and lightRight > (lightLeft - tollerance))) {
      AccLevel = 0;
      Serial.print("\tSTOP:X-PAUSE ");
      lcd.print("STOP:X-PAUSE   ");
    } else if (lightRight > lightLeft and degreeX > xMin) {
      degreeX = degreeX - AccLevel;
      Serial.print("\tMOVING:RIGHT ");
      lcd.print("MOVING:RIGHT   ");
    } else if (lightRight < lightLeft and degreeX < xMax) {
      degreeX = degreeX + AccLevel;
      Serial.print("\tMOVING:LEFT ");
      lcd.print("MOVING:LEFT   ");
    } else {
      Serial.print("\tSTOP:X-OFFSET");
      lcd.print("STOP:X-OFFSET  ");
    }
    Serial.print("\tX-Speed: "); Serial.print(AccLevel);
    Serial.print("\tX-Degree: "); Serial.print(degreeX);
    servoX.write(degreeX);
    EEPROM.write( 10, degreeX);
    delay(0);
  }
}

